# Generated from NotificationManager.pro.

cmake_minimum_required(VERSION 3.16)
set( PROJECT_NAME NotificationManager )
project( ${PROJECT_NAME} )

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt6 COMPONENTS Core)
find_package(Qt6 COMPONENTS Gui)
find_package(Qt6 COMPONENTS Widgets)

qt_add_plugin( ${PROJECT_NAME} SHARED )
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "lib")
target_sources( ${PROJECT_NAME} PRIVATE
    ../../Interfaces/Architecture/PluginBase/../iplugin.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstance.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstanceslist.h
    ../../Interfaces/Architecture/PluginBase/../referenceshandler.h
    ../../Interfaces/Architecture/PluginBase/plugin_base.cpp ../../Interfaces/Architecture/PluginBase/plugin_base.h
    ../../Interfaces/Architecture/PluginBase/plugindescriptor.h
    common_notificationmanagermodel.cpp
    notificationmanager.h
    qextendedtimer.cpp qextendedtimer.h
)
target_link_libraries( ${PROJECT_NAME} PUBLIC
    Qt::Core
    Qt::Gui
    Qt::Widgets
)

if(ANDROID)
    target_sources( ${PROJECT_NAME} PRIVATE
        android_notificationmanagermodel.cpp
    )

    target_link_libraries( ${PROJECT_NAME} PUBLIC
        Qt::AndroidExtras
    )
endif()

if(NOT ANDROID)
    target_sources( ${PROJECT_NAME} PRIVATE
        win_motificationmanagermodel.cpp
    )
endif()

set(SHARED_LIBRARY_NAME "${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}")

add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                "${CMAKE_CURRENT_BINARY_DIR}/${SHARED_LIBRARY_NAME}"
                "${CMAKE_CURRENT_BINARY_DIR}/../../../Application/Plugins/${SHARED_LIBRARY_NAME}")
